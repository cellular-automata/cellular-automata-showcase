/*
 * JBoss, Home of Professional Open Source
 * Copyright 2015, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.cellularautomata.showcase.server.ejb;

import javax.ejb.Stateful;

import ca.cellularautomata.core.shared.entity.EntityFactory;
import ca.cellularautomata.showcase.shared.domain.Address;
import ca.cellularautomata.showcase.shared.domain.Contact;
import ca.cellularautomata.showcase.shared.domain.Name;

/**
 * A simple Hello World EJB. The EJB does not use an interface.
 *
 * @author paul.robinson@redhat.com, 2011-12-21
 */
@Stateful
public class GreeterEJB {
	/**
	 * This method takes a name and returns a personalised greeting.
	 *
	 * @param name
	 *            the name of the person to be greeted
	 * @return the personalised greeting.
	 */
	public String sayHello(String name) {

		Contact contact = EntityFactory.create(Contact.class);
		contact.name(EntityFactory.create(Name.class));
		contact.name().firstName(name);
		contact.name().lastName(name);
		contact.address(EntityFactory.create(Address.class)).street("Street");

		return "Hello " + contact.name().firstName() + " " + contact.name().lastName();
	}
}
