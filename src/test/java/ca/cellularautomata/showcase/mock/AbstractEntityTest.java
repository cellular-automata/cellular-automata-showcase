/*
 * CellularAutomata framework
 * Copyright 2015 CellularAutomata.ca
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * Created on Dec 17, 2015
 * @author michaellif
 */
package ca.cellularautomata.showcase.mock;

import org.junit.Assert;
import org.junit.Test;

import ca.cellularautomata.core.shared.entity.EntityFactory;
import ca.cellularautomata.core.shared.repository.EntitySearchCriteria;
import ca.cellularautomata.showcase.server.service.ContactService;
import ca.cellularautomata.showcase.shared.domain.Account;
import ca.cellularautomata.showcase.shared.domain.Address;
import ca.cellularautomata.showcase.shared.domain.Contact;
import ca.cellularautomata.showcase.shared.domain.Name;

public abstract class AbstractEntityTest {

	@Test
	public void testCreateEmployee() {

		Contact contact1 = createContact("Bill", "Moore", "22 John Street");

		Contact contact2 = createContact("Jack", "Davis", "126 Wilson Street");

		Account account = EntityFactory.create(Account.class);
		account.contacts(contact1, contact2);

		Contact contactProto = EntityFactory.createPrototype(Contact.class);

		// Test paths
		Assert.assertEquals("/address/street/", contactProto.address().street().getPath().toString());

		// Check values
		Assert.assertEquals("22 John Street", contact1.getMember(contactProto.address().street().getPath()).toString());

		Assert.assertEquals(2,
				getContactService().getRepository().find((EntitySearchCriteria<Contact>) null).getData().size());

		Assert.assertEquals(2, account.contacts().size());

		Assert.assertEquals(contact1.name().lastName(),
				getContactService().getRepository().find(contact1.getKey()).name().lastName());

	}

	public Contact createContact(String firstName, String lastName, String address) {
		Contact contact = EntityFactory.create(Contact.class);
		contact.name(EntityFactory.create(Name.class));
		contact.name().firstName(firstName);
		contact.name().lastName(lastName);
		contact.address(EntityFactory.create(Address.class)).street(address);

		getContactService().getRepository().add(contact);

		return contact;
	}

	protected abstract ContactService getContactService();

}
