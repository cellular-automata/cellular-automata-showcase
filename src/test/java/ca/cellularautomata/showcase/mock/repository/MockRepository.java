/*
 * CellularAutomata framework
 * Copyright 2015 CellularAutomata.ca
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * Created on Dec 17, 2015
 * @author michaellif
 */
package ca.cellularautomata.showcase.mock.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.Dependent;

import ca.cellularautomata.core.shared.entity.IEntity;
import ca.cellularautomata.core.shared.entity.Key;
import ca.cellularautomata.core.shared.repository.EntitySearchCriteria;
import ca.cellularautomata.core.shared.repository.EntitySearchResult;
import ca.cellularautomata.core.shared.repository.IRepository;
import ca.cellularautomata.showcase.mock.MockQualifier;

@MockQualifier
@Dependent
public class MockRepository<E extends IEntity> implements IRepository<E> {

	private final Map<Key, E> items;

	public MockRepository() {
		items = new HashMap<>();
	}

	@Override
	public void add(E entity) {
		items.put(entity.getKey(), entity);
	}

	@Override
	public E modify(E entity) {
		return items.put(entity.getKey(), entity);
	}

	@Override
	public boolean delete(Key key) {
		items.remove(key);
		return true;
	}

	@Override
	public E find(Key key) {
		return items.get(key);
	}

	@Override
	public EntitySearchResult<E> find(EntitySearchCriteria<E> criteria) {
		return new EntitySearchResult<E>(new ArrayList<E>(items.values()));
	}

}