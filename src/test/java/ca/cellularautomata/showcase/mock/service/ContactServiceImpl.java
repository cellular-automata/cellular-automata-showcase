/*
 * CellularAutomata framework
 * Copyright 2015 CellularAutomata.ca
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 * Created on Dec 17, 2015
 * @author michaellif
 */
package ca.cellularautomata.showcase.mock.service;

import javax.inject.Inject;

import ca.cellularautomata.core.shared.repository.IRepository;
import ca.cellularautomata.showcase.mock.MockQualifier;
import ca.cellularautomata.showcase.server.service.ContactService;
import ca.cellularautomata.showcase.shared.domain.Contact;

public class ContactServiceImpl implements ContactService {

	@MockQualifier
	@Inject
	private IRepository<Contact> repo;

	@Override
	public IRepository<Contact> getRepository() {
		return repo;
	}

	@Override
	public Contact getContactByLastName(String lastname) {
		// TODO Auto-generated method stub
		return null;
	}

}
